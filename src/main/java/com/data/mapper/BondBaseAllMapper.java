package com.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.data.entity.Book;

public interface BondBaseAllMapper extends BaseMapper<Book> {
}
