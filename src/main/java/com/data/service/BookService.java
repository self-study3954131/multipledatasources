package com.data.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.data.entity.Book;
import com.data.mapper.BookMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    @Autowired
    public BookMapper bookMapper;

    @DS("db1")
    public List<Book> db1Book() {
        return bookMapper.selectList(null);
    }

    @DS("db2")
    public List<Book> db2Book() {
        return bookMapper.selectList(null);
    }
}
