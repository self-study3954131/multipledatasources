package com.data.service;

import com.data.entity.Book;
import com.data.mapper.BondBaseAllMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BondBaseAllService {

    public final BondBaseAllMapper bondBaseAllMapper;

    public BondBaseAllService(BondBaseAllMapper bondBaseAllMapper) {
        this.bondBaseAllMapper = bondBaseAllMapper;
    }

    public List<Book> list() {
        return bondBaseAllMapper.selectList(null);
    }

}
