package com.data.comtroller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.data.entity.Book;
import com.data.service.BondBaseAllService;
import com.data.service.BookService;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Slf4j
@Controller
public class BookController {

    private final BookService bookService;

    private final BondBaseAllService bondBaseAllService;

    public BookController(BookService bookService, BondBaseAllService bondBaseAllService) {
        this.bookService = bookService;
        this.bondBaseAllService = bondBaseAllService;
    }

    @GetMapping("/test")
    public void db1Book(){
        List<Book> book1 = bookService.db1Book();
        List<Book> book2 = bookService.db2Book();
        log.info(book1.toString());
        log.info(book2.toString());
    }

    @GetMapping("/bondBaseAll")
    public void bondBaseAll(HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-disposition", "attachment; filename=全信用债.xlsx");
        List<Book> list = bondBaseAllService.list();//把这里换成你自己的集合数据,泛型全部改为你的实体类
        ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).build();
        List<List<Book>> lists = Lists.partition(list, 500000);//500000表示每一个sheet页有500000数据
        for (int i = 0; i < lists.size(); i++) {
            WriteSheet mainSheet = EasyExcel.writerSheet(i, "sheet" + (i + 1)).head(Book.class).build();
            //向sheet写入数据 传入空list这样只导出表头
            excelWriter.write(lists.get(i), mainSheet);
        }
        excelWriter.finish();
        response.flushBuffer();
    }

}